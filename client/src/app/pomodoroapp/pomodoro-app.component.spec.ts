import { TestBed, async } from '@angular/core/testing';
import { PomodoroAppComponent } from './pomodoro-app.component';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                PomodoroAppComponent
            ],
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(PomodoroAppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'pomodoro-client'`, () => {
        const fixture = TestBed.createComponent(PomodoroAppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('pomodoro-client');
    });

    it('should render title in a h1 tag', () => {
        const fixture = TestBed.createComponent(PomodoroAppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Welcome to pomodoro-client!');
    });
});
