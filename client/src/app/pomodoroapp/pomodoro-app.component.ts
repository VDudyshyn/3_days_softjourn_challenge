import { Component } from '@angular/core';

@Component({
    selector: 'app-pomodoro',
    templateUrl: './pomodoro-app.component.html'
})
export class PomodoroAppComponent {
    title = 'pomodoro-client';
}
