import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { PomodoroAppComponent } from './pomodoroapp/pomodoro-app.component';

@NgModule({
    declarations: [
        PomodoroAppComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
    bootstrap: [PomodoroAppComponent]
})
export class PomodoroAppModule { }
