# Hi there. Intro..

Its my 3 day challenge for a new job. See, my skills are kinda questionable, because of specifics of my previous job by I want to show that I am capable of working with requested stuff without outstanding experience with it. Since I am not good enough with presenting myself I am gonna do it in a way I am confident with ... by writing code.

## Context

API on node + angular client + orientDB usage.
1 hour for idea and architecture development + 3 MDE for implementation. ~25h in total, deadline by Friday morning.

In that application I need show my skills in those particular technologies even tho I have no clue how to work with orientDB but for some reason I decided it will be cool to shout 'I prove you wrong' and now I need to stand for my words. Mehhh

## Idea

App for pomodoro technique usage. Not much, to move around but suits well for 3 days time span.

## Features

Need to show my width in that small project.

### Client app.

Gonna show basic skills in working with Angular:
1. components iteractions
2. reactive form handling
3. app state handling
4. working with services
5. spliting app into 2-3 modules. Probably only two, at lease now I have only two in mind.
6. some simple unit tests. Unfortunately for full coverage and integration tests I dont have time.

### node.js API

1. handling client's pomodoro history
2. provide ability to review and export it
3. ability to plan activities for day and time spans for them.
4. dont want to bother with custom authentication so will make one with google OAuth2
5. will add something for orientDB show off
6. will cover stuff with some unit tests

### orientDB

Thats my main concern. No clue what I am gonna do.

### Project setup features

Need to show that I know how to take care about project. And so I need maintain clean git history, docs and tests coverage report.

# Day 1

25-Dec-2018.

Already spent 1h for developing architecture document, idea dev within my head and this readme file.

Still gonna work 8h full hours and today I need to:

    1. [D1T1] Setup project

        estimate:             30m
        actual time spent:    22m

    2. [D1T2] Start working on PomodoroEngine module.
    Need develop basic functinality to run, stop, reset timer, switch to short
    and long breaks.

        estimate:              2h
        actual time spent:     0m

    3. [D1T3] Add feature to change spans duration, add task description,
    show history of recent pomodoros, store it on client-side for now, but within service.

        estimate:              1h
        actual time spent:     0m

    4. Additional hour to cover mistakes during first 3 tasks.

        estimate:              1h
        time used:             0m

    5. [D1T4] Start working on back-end part. Setup core, API rounting and
    GET 'hello world' request processing.

        estimate:             30m
        actual time spent:     0m

    6. [D1T5] Setup CRUD operations for pomodoro history and save it to DB.

        estimate:              2h
        actual time spent:     0m

    7. [D1T6] Use new history functionality on client-side and it should result in
    working tab with user's pomodoro history with ability to observe list of
    'pomodors', ability apply edit/delete options to entites of list, change
    task descriptions.

        estimate:              2h
        actual time spent:     0m

    8. Additional hour to cover up frustation with design since I suck at design and CSS usage.

        estimate:              1h
        time used:             0m

By EOD I need to have implemented pomodoro engine, working timers and working pomodoro history with usage of CRUD options of API.

Good luck to me.

**TODO:** first thing for next day: 30m-1h of refactoring for all that stuff.